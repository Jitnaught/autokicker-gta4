What this mod is?
This is kind of like a ban mod. You set the players you want to never join, and this mod will automatically kick that person when he joins.

How to install it?
Copy the AutoKicker.net.dll and AutoKicker.ini files to the scripts folder in your GTA IV directory.

How to use it?
Set the names you want to ban in the ini file.
Host a game.
You can also set names while in-game by opening the console (the grave ` button), and typing 'setname [an integer to represent what name to change] [what name]'. Ex: 'setname 1 Hacker'.
You can reset the name to nothing/an empty string by typing into the console 'resetname [an integer to represent what name to reset]'. Ex: 'resetname 1'.
You can reset all names by typing 'resetallnames' with no parameters into the console.
You can list the current names that are banned by typing 'listnames'.

Notes:
This mod does not have a toggle feature. If it is installed, it is enabled.
If the player you are trying to ban has changed his name, it will not kick him.

What changed in 1.1?
There are now 25 players able to be kicked.
A new console command added: resetallnames.

Thanks:

* Aru: Scripthook
* HazardX: .NET Scripthook
* IBLOWUPSTUFF 01: For testing (I kicked him a few times lol)

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
