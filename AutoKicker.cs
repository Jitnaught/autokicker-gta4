﻿using GTA;
using System;
using System.IO;

namespace AutoKicker
{
    public class AutoKicker_Script : Script
    {
        string[] names = new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
        GTA.Timer timer1;

        public AutoKicker_Script()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("name1", "");
                Settings.SetValue("name2", "");
                Settings.SetValue("name3", "");
                Settings.SetValue("name4", "");
                Settings.SetValue("name5", "");
                Settings.SetValue("name6", "");
                Settings.SetValue("name7", "");
                Settings.SetValue("name8", "");
                Settings.SetValue("name9", "");
                Settings.SetValue("name10", "");
                Settings.SetValue("name11", "");
                Settings.SetValue("name12", "");
                Settings.SetValue("name13", "");
                Settings.SetValue("name14", "");
                Settings.SetValue("name15", "");
                Settings.SetValue("name16", "");
                Settings.SetValue("name17", "");
                Settings.SetValue("name18", "");
                Settings.SetValue("name19", "");
                Settings.SetValue("name20", "");
                Settings.SetValue("name21", "");
                Settings.SetValue("name22", "");
                Settings.SetValue("name23", "");
                Settings.SetValue("name24", "");
                Settings.SetValue("name25", "");
            }

            reloadNames(true);

            BindConsoleCommand("setname", setName, "- [(integer) which name] [(string) name]");
            BindConsoleCommand("resetname", resetName, "- [(integer) which name]");
            BindConsoleCommand("resetallnames", resetAllNames);
            BindConsoleCommand("listnames", listNames);

            timer1 = new Timer(1000, false);
            timer1.Tick += timer1_Tick;
            timer1.Start();
        }

        private void resetAllNames(ParameterCollection Parameter)
        {
            if (Parameter.Count == 0)
            {
                for (int i = 1; i <= 25; i++)
                {
                    Settings.SetValue("name" + i.ToString(), "");
                }
                Settings.Save();
                Game.Console.Print("Successfully reset all names");
            }
        }

        private void resetName(ParameterCollection Parameter)
        {
            if (Parameter.Count == 1)
            {
                int tempInt = Convert.ToInt32(Parameter[0]);
                if (tempInt < 26 && tempInt > 0)
                {
                    Settings.SetValue("name" + tempInt.ToString(), "");
                    Settings.Save();

                    reloadNames(false);

                    Game.Console.Print("name" + tempInt.ToString() + " has been reset");
                }
                else
                {
                    Game.Console.Print("The number you inputed for which name to reset, was either too high or too low. Please try again");
                }
            }
        }

        private void listNames(ParameterCollection Parameter)
        {
            if (Parameter.Count == 0)
            {
                reloadNames(true);
            }
        }

        private void setName(ParameterCollection Parameter)
        {
            if (Parameter.Count >= 2)
            {
                int tempInt = Convert.ToInt32(Parameter[0]);
                if (tempInt < 26 && tempInt > 0)
                {
                    string tempString = ""; 
                    for (int i = 1; i < Parameter.Count; i++)
                    {
                        string s = Convert.ToString(Parameter[i]);
                        if (i != 1) s = " " + s;
                        tempString += s;
                    }

                    Settings.SetValue("name" + tempInt.ToString(), tempString);
                    Settings.Save();

                    reloadNames(false);

                    Game.Console.Print("name" + tempInt.ToString() + " = " + tempString);
                }
                else
                {
                    Game.Console.Print("The number you inputed for which name to set, was either too high or too low. Please try again");
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (imInGame())
            {
                timer1.Interval = 1000;

                if (Game.isMultiplayer)
                {
                    if (GTA.Multiplayer.Host.isHost)
                    {
                        Player me = Game.LocalPlayer;
                        Player[] players = Game.PlayerList;

                        foreach (Player p in players)
                        {
                            string pName = p.Name;
                            if (playerInGame(p))
                            {
                                if (p != me)
                                {
                                    bool kick = false;

                                    foreach (string s in names)
                                    {
                                        if (pName == s)
                                        {
                                            kick = true;
                                            break;
                                        }
                                    }

                                    if (kick)
                                    {
                                        try
                                        {
                                            KickPlayer(p);
                                        }
                                        catch (Exception) { }

                                        Game.Console.Print("* Kicked " + pName);
                                        Game.DisplayText("* Kicked " + pName);
                                    }
                                }
                            }
                            else
                            {
                                Game.Console.Print(pName + " is not in game, so he has not been kicked");
                            }
                        }
                    }
                }
            }
            else
            {
                timer1.Interval = 200;
            }
        }

        private bool imInGame()
        {
            bool inGame = true;
            if (GTA.Native.Function.Call<bool>("IS_SCREEN_FADED_OUT"))
            {
                inGame = false;
            }
            return inGame;
        }

        private bool playerInGame(Player player)
        {
            bool inGame = false;
            if (player != null && player.Character != null && !Player.Character.isDead && !GTA.Native.Function.Call<bool>("HAS_NETWORK_PLAYER_LEFT_GAME", player) && player.isPlaying && player.isActive)
            {
                inGame = true;
            }
            return inGame;
        }

        private void reloadNames(bool printNames)
        {
            if (printNames) Game.Console.Print("---Start of names---");

            for (int i = 0; i < names.Length; i++)
            {
                string tempString = Settings.GetValueString("name" + (i + 1).ToString());
                names[i] = tempString;

                if (printNames && !string.IsNullOrWhiteSpace(names[i])) Game.Console.Print(names[i]);
            }

            if (printNames) Game.Console.Print("---End of names---");
        }

        private void KickPlayer(Player player)
        {
            GTA.Native.Function.Call("NETWORK_KICK_PLAYER", player, true);
        }
    }
}
